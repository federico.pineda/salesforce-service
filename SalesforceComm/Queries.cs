﻿using Newtonsoft.Json;
using SalesforceComm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace SalesforceComm
{
    public class Queries
    {
        public IEnumerable<string> GetAccounts(string token)
        {
            try
            {
                using (var client = new HttpClient())
                {

                    string restQuery = "https://na134.salesforce.com/services/data/v46.0/sobjects/";
                    var request = new HttpRequestMessage(HttpMethod.Get, restQuery);
                    request.Headers.Add("Authorization", "Bearer " + token);
                    request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    request.Headers.Add("X-PrettyPrint", "1");
                    var response = client.SendAsync(request).Result;

                    var objectList = new List<string>();


                    var sObjectList = JsonConvert.DeserializeObject<SObjectDescribe>(response.Content.ReadAsStringAsync().Result);
                    foreach (var sobject in sObjectList.sobjects)
                    {
                        if (sobject.name == "Contact" || sobject.name == "Case" || sobject.name == "Group" || sobject.name == "Lead" || sobject.name == "Opportunity")
                        {
                            objectList.Add(sobject.name);
                        }

                    }

                    return objectList;

                }
            }
            catch (Exception ex)
            {
                var objectList = new List<string>();
                objectList.Add("null");                
                throw;
            }
            
        }
    }
}
