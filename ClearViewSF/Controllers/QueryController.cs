﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ClearViewSF.Controllers
{
    public class QueryController : ApiController
    {
        private SalesforceComm.Queries comm = new SalesforceComm.Queries();

        [HttpGet]
        [Route("saleforce/query")]
        public IEnumerable<string> Query ()
        {
            string token = "";
            return comm.GetAccounts(token);
        }
    }
}
