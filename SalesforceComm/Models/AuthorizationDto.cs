﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesforceComm.Models
{
    public class AuthorizationDto
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("scope")]
        public string Scope { get; set; }

        [JsonProperty("instance_url")]
        public Uri InstanceUrl { get; set; }

        [JsonProperty("id")]
        public Uri Id { get; set; }

        [JsonProperty("token_type")]
        public string TokenType { get; set; }
    }
}
