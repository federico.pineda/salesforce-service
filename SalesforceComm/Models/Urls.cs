﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesforceComm.Models
{
    public class Urls
    {
        public string compactLayouts { get; set; }
        public string rowTemplate { get; set; }
        public string approvalLayouts { get; set; }
        public string uiDetailTemplate { get; set; }
        public string uiEditTemplate { get; set; }
        public string defaultValues { get; set; }
        public string describe { get; set; }
        public string uiNewRecord { get; set; }
        public string quickActions { get; set; }
        public string layouts { get; set; }
        public string sobject { get; set; }
        public string listviews { get; set; }
    }
}
