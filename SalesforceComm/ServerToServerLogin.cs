﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Cryptography.X509Certificates;
using System.Net.Http;
using Newtonsoft.Json;
using SalesforceComm.Models;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;

namespace SalesforceComm
{
    public class ServerToServerLogin
    {
        static string path = @"C:\Users\fedep\Source\Repos\ClearViewSF\SalesforceComm\certificates\";
        public AuthorizationDto LoginOAuth(string ClientId, string ClientSecret, string Username, string Password, string SecurityToken, string Login_Endpoint)
        {
            try
            {
                string JsonResponse;
                string jwt = CreateJwt(ClientId, ClientSecret, Username, Password, SecurityToken, Login_Endpoint);
                var dic = new Dictionary<string, string>
            {
                { "grant_type", "urn:ietf:params:oauth:grant-type:jwt-bearer" },
                { "assertion", jwt }
            };

                var content = new FormUrlEncodedContent(dic);

                var httpClient = new HttpClient { BaseAddress = new Uri("https://login.salesforce.com") };
                var response = httpClient.PostAsync("/services/oauth2/token", content).Result;
                JsonResponse = response.Content.ReadAsStringAsync().Result;
                string cleanObject = Regex.Unescape(JsonResponse);
                var result = JsonConvert.DeserializeObject<AuthorizationDto>(JsonResponse);

                dynamic dyn = response.Content.ReadAsAsync<dynamic>();
                return result;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private static string CreateJwt(string ClientId, string ClientSecret, string Username, string Password, string SecurityToken, string LOGIN_ENDPOINT)
        {

            DateTime now = DateTime.UtcNow;
            long exps = DateTimeOffset.UtcNow.AddMinutes(3).ToUnixTimeSeconds();


            var payload = new JwtPayload
            {
                {"iss", ClientId},
                {"sub",  Username},
                {"aud", "https://login.salesforce.com"},
                {"exp",  exps.ToString()},
            };


            var headers = new Dictionary<string, object>()
            {
                { "alg", "RS256" }
            };

            try
            {
                string certificateText = System.IO.File.ReadAllText(path + "salesforce.crt");
                string privateKeyText = System.IO.File.ReadAllText(path + "salesforce.key");

                OpenSSL.X509Certificate2Provider.ICertificateProvider provider = new OpenSSL.X509Certificate2Provider.CertificateFromFileProvider(certificateText, privateKeyText);
                X509Certificate2 certificates = provider.Certificate;

                var tokensa = Jose.JWT.Encode(payload, certificates.PrivateKey, Jose.JwsAlgorithm.RS256, headers);


                return String.Join(".", tokensa);
            }
            catch (Exception ex)
            {

                throw;
            }
        }


    }
}
