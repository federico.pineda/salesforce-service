﻿using Newtonsoft.Json;
using SalesforceComm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;

namespace ClearViewSF.Controllers
{
    public class AuthorizationSFController : ApiController
    {
        private SalesforceComm.ServerToServerLogin comm = new SalesforceComm.ServerToServerLogin();

        [HttpGet]
        [Route("saleforce/authorization")]
        public AuthorizationDto AuthorizationServerToServer()
        {
            return comm.LoginOAuth("[ClientId]", "[ClientSecret]", "[Username]", "[UserPassword]", "test", "https://login.salesforce.com/services/oauth2/authorizeee");
        }
    }
}
