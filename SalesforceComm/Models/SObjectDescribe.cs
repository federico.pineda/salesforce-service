﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesforceComm.Models
{
    public class SObjectDescribe
    {
        public string encoding { get; set; }
        public int maxBatchSize { get; set; }
        public Sobject[] sobjects { get; set; }
    }
}
